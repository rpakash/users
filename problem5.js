function problem5(users) {
  if (!Array.isArray(users)) {
    return [];
  }

  const maleArray = [];

  for (let index = 0; index < users.length; index++) {
    const user = users[index];
    if (user.gender === "Male") {
      maleArray.push({
        email: user.email,
        first_name: user.first_name,
        last_name: user.last_name,
      });
    }
  }

  return maleArray;
}

module.exports = problem5;
