function problem1(users, id) {
  if (!Array.isArray(users) || typeof id !== "number") {
    return [];
  }

  let user = [];

  for (let index = 0; index < users.length; index++) {
    if (users[index].id == id) {
      user = users[index];
      break;
    }
  }

  return user;
}

module.exports = problem1;
