function problem3(users) {
  if (Array.isArray(users)) {
    users.sort((userA, userB) =>
      userA.last_name.localeCompare(userB.last_name)
    );
    return users;
  }

  return [];
}

module.exports = problem3;
