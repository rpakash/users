function problem4(users) {
  if (!Array.isArray(users)) {
    return [];
  }

  const emails = [];

  for (let index = 0; index < users.length; index++) {
    emails.push(users[index].email);
  }

  return emails;
}

module.exports = problem4;
