function problem6(users) {
  if (!Array.isArray(users)) {
    return [];
  }

  const gendersArray = [];
  const gendersList = {
    Male: 0,
    Female: 1,
    Polygender: 2,
    Bigender: 3,
    Genderqueer: 4,
    Genderfluid: 5,
    Agender: 6,
  };

  for (let index = 0; index < users.length; index++) {
    let gender = users[index].gender;
    gendersArray[gendersList[gender]]
      ? gendersArray[gendersList[gender]].push(users[index])
      : (gendersArray[gendersList[gender]] = [users[index]]);
  }
  return gendersArray;
}

module.exports = problem6;
