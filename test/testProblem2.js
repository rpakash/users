const problem2 = require("../problem2");
const users = require("../users");

const user = problem2(users);

console.log(
  Array.isArray(user)
    ? []
    : `Last user is ${
        user.first_name + " " + user.last_name
      } and can be contacted on ${user.email}`
);
