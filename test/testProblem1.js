const problem1 = require("../problem1");
const users = require("../users");

const user = problem1(users, 100);

console.log(
  Array.isArray(user)
    ? []
    : `${user.first_name + " " + user.last_name} is a ${
        user.gender
      } and can be contacted on ${user.email} `
);
