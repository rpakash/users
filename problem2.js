function problem2(users) {
  if (Array.isArray(users) && users.length > 0) {
    return users.at(-1);
  } else {
    return [];
  }
}

module.exports = problem2;
